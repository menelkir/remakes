Welcome to the Brain Games remake of the 8 bit classic game 'The Goonies'. This game was made by Konami in 1986. There were both a NES version and an MSX version of the game; this remake is based on the MSX version.

This remake was made for the 2006 competition organized by Retro Remakes. After having participated in 2003 (with Road Fighter, which finished on the 7th place out of 83 entries), and in 2004 (with F-1 Spirit, gaining the 13th place amongst the 73 contestants), we decided to give another go at the first prize! Even though we didn't make it to the first place, we are still happy with a 5th place (out of 76)!

----

Forums and support

If you have any problems, questions or suggestions about The Goonies, feel free to join us at the Braingames forums, or even better, at the Goonies forum. The forum is the place where you can find the latest developments, send us your contributions, send us fanmail or have just a friendly chat. You're welcome to join us!

----

Story

The action takes place in the sleepy seaside town of Cauldron Point - down in an area known as the Goon Docks. The Goonies are a group of local kids - Mikey, Brand, Mouth, Chunk and Data. One day they find an old treasure map in Mikey's attic. Data figures that this must be the fabulous hidden treasure of the notorious local pirate, One-Eyed Willy. The girls, Andy and Stef, join the other Goonies and the adventure begins. But the secret underground tunnels they are exploring are actually the hideout of the Fratelli Gang, and the Goonies are soon trapped! The Goonies have a powerful ally on their side, however, in the form of the huge and amiable Sloth.

When you play this game, you are Sloth! Can you help the Goonies find the treasure and escape back to save the Goon Docks from demolition?

----


How to play

When you have started the game, you can select the 'How to play' menu option for a visual explanation about the game's controls and objectives. There's also an explanation of the available items and what they do in the 'How to play' section. These instructions are an expanded version of the in-game menu.
General guidelines

Here's some points worth noting:

    This game is designed for a single player competing against the computer
    The action can be controlled from the keyboard of your computer, or with a joystick/gamepad
    Press the Space Bar on your keyboard to start the game
    An interesting feature of this game is that you have to keep your player's level of VITALITY and EXPERIENCE in mind. You start off with 100% vitality and zero experience. These levels are indicated on your computer screen
    As you proceed through the game and run into obstacles or enemies, your vitality naturally decreases
    On the other hand, your level of experience will increase as you successfully dispose of the horrors (like the skulls and the skeletons) you meet in the underground maze. When your experience level reaches 100%, your vitality will increase
    You can knock out the members of the Fratelli Gang, but remember: you won't gain any EXPERIENCE from this!
    Once you succeed in rescuing all seven of the Goonies from the underground maze of tunnels, the next stage of the game will begin
    You can also increase your vitality level by grabbing up any bottles of VITALITY DRINK you see in an empty cage
    To pause the game momentarily, press the [F-1] key. This will freeze the screen and show the map. Pressing the [F-1] key again will restart the game

Controls

These are the default controls of the game. They can be redefined in the options menu.
Key 	Action 	Info
Cursor up 	Jump up, climb up vines 	
Cursor down 	Climb down 	
Cursor left 	Walk to the left 	
Cursor right 	Walk to the right 	
Space 	Punch 	Can also be done while jumping
F-1 	Pause 	
ESC 	Back to title screen 	
CTRL+K 	Enter password 	Only during title screen
ALT+ENTER 	Switch between full-screen and window mode 	For Windows and Linux
Apple key+F 	Switch between full-screen and window mode 	Only for Mac OS X
F12 	Quit 	For Windows and Linux
Apple key+Q 	Quit 	Only for Mac OS X
ALT+F 	Show FPS 	
Characters and components
Sloth
sloth

Sloth is the main character. He's the gentle gigantic creature who helps the Goonies to find the treasure and escape from the labyrinth.
Fratelli Gang members
ma fratelli francis fratelli jake fratelli

These are the bad guys. They want to hunt down and capture all the Goonies, who have stumbled in on the Gang's secret hideout. They are armed and dangerous!
Keys
key

These keys are to the cages where the Fratelli Gang has the Goonies locked up. Be careful, a cage may have more than one keyhole.
Doubloons
doubloon

Gold coins from the treasure hoard of One-Eyed Willy. Use these to increase your score.
Treasure sacks
treasure sack

Contained in these are many devices to help save the Goonies. But, be careful: sometimes they contain something you don't want at all!
But there's more!

Other items of treasure include... Shields, magic formulas, riddles, hyper shoes, helmets, torches, time stopping devices, and magic capes. These have different effects on the enemy, and the same item will change effects according to its color. Figure out what must be done to get these treasure items to appear!

----

Credits

This remake was made for the Retro Remakes 2006 competition by Brain Games. The game was made in the months June, July and August of 2006 by the following people:
Coding

    Santi Ontanon
    Jorrith Schaap
    Patrick Lina
    Nene Franz
    David Fernandez

Graphics

    David Fernandez
    Miikka Poikela
    Daedalus
    Kelesisv
    Nene Franz

Music & SFX

    Jorrith Schaap

Maps

    Jesus Perez Rosales
    Mauricio Braga
    Patrick Lina
    Santi Ontanon

Beta testing

    All of the above
    Albert Beevendorp
    Patrick van Arkel

